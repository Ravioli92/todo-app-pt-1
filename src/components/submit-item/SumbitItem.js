import React, { Component } from "react"

class SubmitItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      submitted: false,
      newTodo: "",
    }
  }

  handleChange = (e) => {
    console.log(e.target.value)

    this.setState({ newTodo: e.target.value })
  }

  render() {
    return (
      <div>
        <h1>todos</h1>
        <input
          className="new-todo"
          type="text"
          name="addTask"
          placeholder="What needs to be done?"
          autofocus
          value={this.state.newTodo}
          onChange={this.handleChange}
        />
        <button
          className="submitButton"
          onClick={() => {
            this.props.addTodo(this.state.newTodo)
            this.setState({ newTodo: "" })
          }}
        >
          Submit
        </button>
      </div>
    )
  }
}

export default SubmitItem
