import React, { Component } from "react"
import TodoItem from "../todo-item/TodoItem"

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              id={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleToggle={this.props.handleToggle}
              destroy={this.props.destroy}
            />
          ))}
        </ul>
      </section>
    )
  }
}

export default TodoList
