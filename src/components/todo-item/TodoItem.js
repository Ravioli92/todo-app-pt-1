import React, { Component } from "react"

class TodoItem extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick={() => this.props.handleToggle(this.props.id)}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={() => this.props.destroy(this.props.id)}
          />
        </div>
      </li>
    )
  }
}

export default TodoItem

// created onchange to activate handletoggle
// passed down id prop from app.js
