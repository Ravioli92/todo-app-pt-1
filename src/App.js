import React, { Component } from "react"
import todosList from "./todos.json"
import TodoList from "./components/todo-list/TodoList"
import SubmitItem from "./components/submit-item/SumbitItem"

class App extends Component {
  state = {
    todos: todosList,
    count: 4,
    newTodoId: 6,
  }

  handleToggle = (todoId) => {
    const { todos } = this.state

    todos.forEach((item) => {
      if (todoId === item.id) {
        item.completed = !item.completed
        if (item.completed) {
          this.setState({ count: this.state.count - 1 })
        } else {
          this.setState({ count: this.state.count + 1 })
        }
      }
    })

    this.setState({ todos })

    // made copy of this.state.todos;
    // looped through or filter or something to update the todo with an id matching parameter 'todoId'
    // set state of this.state.todos to === modified copy of todo list
  }

  addTodo = (newTodoText) => {
    const { todos } = this.state
    const todoObj = {
      userId: 1,
      completed: false,
      id: this.state.newTodoId,
      title: newTodoText,
    }
    todos.push(todoObj)

    this.setState({
      newTodoId: this.state.newTodoId + 1,
      todos,
      count: this.state.count + 1,
    })
  }

  clearCompleted = () => {
    let newArr = []
    const { todos } = this.state
    todos.forEach((item) => {
      if (!item.completed) {
        newArr.push(item)
      }
      this.setState({ todos: newArr })
    })
  }

  destroy = (todoId) => {
    let newArr = []
    const { todos } = this.state
    todos.forEach((item) => {
      if (todoId !== item.id) {
        newArr.push(item)
      }
    })
    this.setState({ count: this.state.count - 1 })
    this.setState({ todos: newArr })
  }
  // fix destroy handler

  render() {
    return (
      <section className="todoapp">
        <SubmitItem addTodo={this.addTodo} />
        <TodoList
          handleToggle={this.handleToggle}
          todos={this.state.todos}
          destroy={this.destroy}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.count}</strong> item(s) left
          </span>
          <button onClick={this.clearCompleted} className="clear-completed">
            Clear completed
          </button>
        </footer>
      </section>
    )
  }
}

export default App
